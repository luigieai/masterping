# masterPing
Plugin cujo objetivo é expulsar jogadores com latência alta, contando com configuração de mensagens e da funcionalidade do plugin.
 O plugin conta com um comando e tem um total de 2 permissões.
 
 **Download [aqui](https://bitbucket.org/luigieai/masterping/downloads)**
 
 **Funcionalidade**
  A cada __X__ segundos (determinado na config) o plugin irá identificar todos os jogadores com latência maior que __x__ ms (determinado na config) e irá expulsar eles do seu servidor com uma mensagem pré definida
   Quando um jogador entrar, depois de 10 segundos será checado se o jogador está com latência alta, se tiver, será expulso do servidor.
 
 **Comandos**
  */ping - Manda uma mensagem mostrando a sua latência. permissão: __ping.self__
  */ping <jogador> - Manda uma mensagem mostrando a latência do jogador citado no argumento. permissão: __ping.others__
  */ping forcecheck - Faz uma checagem para encontrar algum jogador lagado e expulsar ele. permissão: __ping.check__
  
 **Sistema de log**
  O plugin conta com um sistema de log, a cada vez que o jogador for expulso porcausa do lag, irá ser registrado num arquivo txt com a data atual do servidor, a hora que o jogador foi expulso será registrada também!

 **Em breve API**
 
 **VERSÃO: 1.5 a 1.8**