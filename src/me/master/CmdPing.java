package me.master;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdPing implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			String[] args) {
		if(cmd.getName().equalsIgnoreCase("ping")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				if(args.length == 0){
					if(!p.hasPermission("ping.self")){
						p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.noPerm")
								.replace("&", "�"));
						return true;
					}
					
					p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.pingPlayer")
							.replace("&","�")
							.replace("%ping%", ""+Util.pingPlayer(p)));
				}
				
				if(args.length == 1){
					
					if(args[0].equalsIgnoreCase("forcecheck")){
						if(!p.hasPermission("ping.check")){
							p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.noPerm")
									.replace("&", "�"));
							return true;
						}
						p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.check")
								.replace("&", "�"));
						Util.pingAll();
						return true;
					}
					
					if(!p.hasPermission("ping.others")){
						p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.noPerm")
								.replace("&", "�"));
						return true;
					}
					
					Player t = Bukkit.getPlayer(args[0]);
					if(t == null){
						p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.notFound")
								.replace("&", "�")
								.replace("%player%", args[0]));						
						return true;
					}
					
					else{
						p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.pingOther")
								.replace("&", "�")
								.replace("%ping%", ""+Util.pingPlayer(t))
								.replace("%player%", t.getName()));					
					}
				}
				
				if(args.length >=2){
					p.sendMessage(Main.getPlugin(Main.class).getConfig().getString("mensagens.noArgs")
							.replace("&", "�"));				
					}
			}
		}
		return false;
	}

}
