package me.master;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class Main extends JavaPlugin implements Listener {

	/**
	 * @author MasterZ (Luigi)
	 * TODO:
	 *    - API
	 */
	
	public void onEnable(){
		saveDefaultConfig();
		getCommand("ping").setExecutor(new CmdPing());
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Util.updateConfig(this);
		startLog();
		startRunnable();
		System.out.println("Plugin masterPing ativado, feito por L�igi 'MasterZ', twitter: @LuigiOliveira__");
	}
	public void onDisable(){
		System.out.println("Plugin masterPing desativado, feito por L�igi 'MasterZ', twitter: @LuigiOliveira__");
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		new BukkitRunnable(){ 
			@Override
			public void run() {
				int e = Util.pingPlayer(p);
				if(e >= getConfig().getInt("core.pingPraKikar")){
	    			p.kickPlayer(Main.getPlugin(Main.class).getConfig().getString("mensagens.kikado")
	    					.replace("&", "�")
	    					.replace("%ping%", e+""));
	    			Util.logToFile(p.getName()+" foi kikado as: "+Util.getHora(), Util.getData());
	    			return;
				}
				/*
				 * Checar se o ping est� pr�ximo
				 */
				int z = getConfig().getInt("core.pingPraKikar");
				for(int i = (z-10); i>=z;i++){
				   	if(i == e){
				   		p.sendMessage(getConfig().getString("mensagens.pingWarn").replace("&", "�"));
				   	}
				}
			}
			
		}.runTaskLater(this, 20*10);
	}
	
    private void startLog(){
    	System.out.println("Inicializando sistema de log!");
    	Util.logToFile("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", Util.getData());
    	System.out.println("Inicializado!");
    }
    
    private void startRunnable(){
		new BukkitRunnable(){
			
			@Override
			public void run() {
				Util.pingAll();
			}
			
		}.runTaskTimer(this, 20*10, 20*getConfig().getInt("core.tempoChecagem"));
    }
}
