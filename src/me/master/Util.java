package me.master;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Util {
	
    static public int pingPlayer(Player qem) {
        try {
            String bukkitversion = Bukkit.getServer().getClass().getPackage()
                    .getName().substring(23);
            Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit."
                    + bukkitversion + ".entity.CraftPlayer");
            Object handle = craftPlayer.getMethod("getHandle").invoke(qem);
            Integer ping = (Integer) handle.getClass().getDeclaredField("ping")
                    .get(handle);
            return ping.intValue();
        } catch (ClassNotFoundException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException
                | NoSuchFieldException e) {
            return -1;
		}
	}
    
    @SuppressWarnings("deprecation")
	static public void pingAll(){
    	for(Player p : Bukkit.getOnlinePlayers()){
    		int e = pingPlayer(p);
    		if(e >= Main.getPlugin(Main.class).getConfig().getInt("core.pingPraKikar")){
    			p.kickPlayer(Main.getPlugin(Main.class).getConfig().getString("mensagens.kikado")
    					.replace("&", "�")
    					.replace("%ping%", e+""));
    		}
    	}
    }
    
	static public void logToFile(String message,String file)
	 
    {
 
        try
        {
            File dataFolder = Main.getPlugin(Main.class).getDataFolder();
            if(!dataFolder.exists())
            {
                dataFolder.mkdir();
            }
 
            File saveTo = new File(Main.getPlugin(Main.class).getDataFolder(), file+".txt");
            if (!saveTo.exists())
            {
                saveTo.createNewFile();
            }
 
 
            FileWriter fw = new FileWriter(saveTo, true);
 
            PrintWriter pw = new PrintWriter(fw);
 
            pw.println(message);
 
            pw.flush();
 
            pw.close();
 
        } catch (IOException e){
 
            e.printStackTrace();
 
        }
 
    }
	
	static public String getData(){
		DateFormat f = new SimpleDateFormat("dd/MM");
		Date d = new Date();
		return f.format(d);
	}
	
	static public String getHora(){
		DateFormat f = new SimpleDateFormat("HH:mm:ss");
		Date d = new Date();
		return f.format(d);
	}
	
	public static void updateConfig(Plugin pl){
		System.out.println("Checando se a config.yml esta atualizada!");
		checkPath("core.pingPraKikar",100,pl);
		checkPath("core.tempoChecagem",120,pl);
		checkPath("mensagens.noPerm","�cVoc� n�o tem permiss�o para isso!",pl);
		checkPath("mensagens.pingPlayer","&6Seu ping: &a&b%ping%",pl);
		checkPath("mensagens.pingOther","�6O player �a%player% �6esta com ping de: �b%ping%",pl);
		checkPath("mensagens.notFound","�c%player% n�o foi encontrado!",pl);
		checkPath("mensagens.noArgs","�cArgumentos invalidos! Use /ping ou /ping <jogador>",pl);
		checkPath("mensagens.kikado","�cVoc� foi kikado por ter o ping de %ping%",pl);
		checkPath("mensagens.check","�aVoc� for�ou uma checagem por players lagado!",pl);
		checkPath("mensagens.pingWarn","�cVoc� esta com um ping alto! Cuidado sen�o sera kikado!",pl);
		pl.getConfig().options().copyDefaults(true);
		pl.saveConfig();
        System.out.println("Feito!");
	}
	
	private static void checkPath(String path,Object Value,Plugin pl){
		if(!pl.getConfig().contains(path)){
			pl.getConfig().addDefault(path,Value);
		}
	}
}
